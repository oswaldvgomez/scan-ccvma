import React from "react";

export class Register extends React.Component {


  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="form">
            <div className="form-group">
              <label htmlFor="username"></label>
              <input type="text" name="username" placeholder="Nombre de usuario" />
            
              <label htmlFor="password"></label>
              <input type="text" name="password" placeholder="Contraseña" />
            
              <label htmlFor="question"></label>
              <input type="text" name="question" placeholder="Pregunta de seguridad" />
            
              <label htmlFor="answer"></label>
              <input type="text" name="answer" placeholder="Respuesta de seguridad" />
            </div>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btn">
            Registrar
          </button>
        </div>
      </div>
    );
  }
}