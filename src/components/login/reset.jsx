import React from "react";

export class Reset extends React.Component {

    render() {
        return(
            <div className="base-container" ref={this.props.contrainerRef}>
                <div className="header">Log in</div>
                <div className="content">
                    <div className="form">
                        <div className="form-group">

                            <label htmlFor="username"></label>
                            <input type="text" name="username" placeholder="Nombre de usuario"/>

                            <label htmlFor="question"></label>
                            <input type="text" name="question" placeholder="Aquí va la pregunta"/>

                            <label htmlFor="answer"></label>
                            <input type="password" name="password" placeholder="Respuesta"/>

                            <label htmlFor="password"></label>
                            <input type="password" name="password" placeholder="Contraseña nueva"/>

                            <label htmlFor="password"></label>
                            <input type="password" name="password" placeholder="Confirmar contraseña"/>
                            
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <button type="submit" className="btn">Listo</button><br></br>
                </div>
            </div>
        )
    }
}