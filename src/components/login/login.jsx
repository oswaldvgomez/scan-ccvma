import React from 'react';

export class Login extends React.Component {

    

    render() {
        return(
            <div className="base-container" ref={this.props.contrainerRef}>
                <div className="header">Log in</div>
                <div className="content">
                    <div className="form">
                        <div className="form-group">

                            <label htmlFor="username"></label>
                            <input type="text" name="username" placeholder="Nombre de usuario"/>
                       
                            <label htmlFor="password"></label>
                            <input type="password" name="password" placeholder="Contraseña"/>

                        </div>
                    </div>
                </div>
                <div className="footer">

                    <button className="btn reset-pass">Olvidé contraseña</button>
                    <button type="submit" className="btn">Entrar</button>

                </div>
            </div>
        )
    }
}